import React, { Component } from 'react';
import axios from 'axios';
import Zoom from 'react-reveal/Zoom';
import Flip from 'react-reveal/Flip'

class App extends Component {
  constructor() {
    super();
    this.state = {
      datosob: [],
    };
}

klikPost(e){
  e.preventDefault();
  var url = 'http://localhost:3210/data';
  axios.post(url, {
    nombre: this.inputnombre.value,
    edad: this.inputedad.value
  })
  .then(function (response) {
    console.log(response);
  })
  .catch(function (error) {
    console.log(error);
  });
  this.inputnombre.value = '';
  this.inputedad.value = '';
};

klikGet(e){
  e.preventDefault();
  var url = 'http://localhost:3210/data';
  axios.get(url)
  .then((ambilData) => {
    console.log(ambilData.data);
    this.setState({
      datosob: ambilData.data,
    }) 
  })
};

render() {
  const dataMongo = this.state.datosob.map((item, index)=>{
    var arrayku = ['nombre: ',item.nombre,', edad: ', item.edad, ' th.'].join(' ');
    return <p key={index}>{arrayku}</p>;
  })
  return (
   <div className="container">
   <Zoom>
     <center style={{margin:'25px'}}>
        <Flip><h3>React Express MongoDB</h3></Flip>
     
     <form>

  <div className="form-group" style={{margin:'15px'}}>
    <input className="form-control" type="text" id="nombre" 
    ref={ innombre => this.inputnombre = innombre }
    placeholder="nombre"/>
  </div>

  <div className="form-group" style={{margin:'15px'}}>
    <input className="form-control" type="number" id="edad" 
    ref={ inedad => this.inputedad = inedad }
    placeholder="edad"/>
  </div>
  
  <button className="btn btn-primary" style={{width:'100px'}}
  onClick={this.klikPost.bind(this)}>Agregar</button>
  
  <button className="btn btn-success" style={{margin:'15px',width:'100px'}}
  onClick={this.klikGet.bind(this)}>Mostrar</button>

</form>

     <div>
       { dataMongo }
     </div>
     </center>
     </Zoom>
   </div>
  );
 }
 }
 
export default App;